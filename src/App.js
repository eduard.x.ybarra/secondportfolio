import React, { useState, useEffect, useRef, useCallback } from 'react';
import './App.css';
import Navbar from './hookcomponents/Navbar.jsx'; 
import Home from './hookcomponents/Home.jsx'; 
import About from './hookcomponents/About.jsx';
import Projects from './hookcomponents/Projects.jsx';
import Contact from './hookcomponents/Contact.jsx';
import SectionSeperator from './hookcomponents/SectionSeperator.jsx';
import aboutDataJson from './data/aboutData.json';
import projectsDataJson from './data/projectsData.json';

function App() {
  const [aboutData, setAboutData] = useState([]);
  const [projectsData, setProjectsData] = useState([]);

  useEffect(() => {
    const loadData = () => {
      const loadedAboutData = [];
      const loadedProjectsData = [];

      for(let data of aboutDataJson) {
        loadedAboutData.push(
          data
        );
      }

      for(let data of projectsDataJson) {
        loadedProjectsData.push(
          data
        );
      }

      setAboutData(loadedAboutData);
      setProjectsData(loadedProjectsData);
    }
    loadData();
  }, []);

  if(aboutData[0] === undefined) {
    return (
      <div>Loader</div>
    )
  }

  return (
    <div className="App">
      <Navbar />
      <div id={"test1"}>
        <Home />
      </div>
      <div id={"test2"}>
        <SectionSeperator topColor={"red"} bottomColor={"blue"}/>
        <About aboutData={aboutData} />
      </div>
      <div id={"test3"}>
        <SectionSeperator topColor={"blue"} bottomColor={"green"}/>
        <Projects projectsData={projectsData} />
      </div>
      <div id={"test4"}>
        <SectionSeperator topColor={"green"} bottomColor={"yellow"}/>
        <Contact />
      </div>
    </div>
  );
}

export default App;