import React, { useRef, useCallback } from 'react';
import '../css/SectionSeperator.css';

import gsap from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';
gsap.registerPlugin(ScrollTrigger);

const SectionSeperator = ({ topColor, bottomColor }) => {
  const main = useCallback(node => {

  if (node !== null) { 
    gsap.to(indicator.current, {
      scrollTrigger: {
        trigger: indicator.current,
        start: "top center",
        end: "top 100px",
        scrub: true
      },
      y: "134px"
    })
    
    gsap.to(line.current, {
      scrollTrigger: {
        trigger: line.current,
        start: "top center",
        end: "top 100px",
        scrub: true
      },
      backgroundColor: `${bottomColor}`
    })

  } else {

  }
  }, []);
  const line = useRef();
  const indicator = useRef();

  return (
    <div ref={main} className="project-seperator">
      <div ref={line} className="seperator-line" style={{backgroundColor: `${topColor}`}}>
        <div ref={indicator} className="line-indicator"></div>
      </div>
    </div>
  )
}

export default SectionSeperator;