import React from 'react';
import Tilt from 'react-parallax-tilt';

import '../css/Experience.css';

import { faDigging, faBriefcase, faIndustry } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Experience = () => {
  return (
    <Tilt transitionSpeed={5000} tiltEnable={false} glareEnable={true} glareMaxOpacity={0.1} glareColor="white" glarePosition='all' className={"experience-section"}>
      <div className={"experience-title"}>
        <div className={"experience-icon-box"}>
          <FontAwesomeIcon icon={faBriefcase} size="2x"/>
        </div>
        <p>Experience</p>
      </div>
      <div className={"small-experience-title"}>
        <div className={"small-experience-icon-box"}>
          <FontAwesomeIcon icon={faIndustry} size="1x"/>
        </div>
        <p>Jobs</p>
      </div>
      <div className={"experience-part"}>
        <div className={"experience-year"}>
          <p>2014-2015</p>
        </div>
        <div className={"experience-description"}>
          <strong>Coca Cola</strong>
          <p>
            Preperation and Mixing
          </p>
        </div>
      </div>
      <div className={"experience-part"}>
        <div className={"experience-year"}>
          <p>2020-2021</p>
        </div>
        <div className={"experience-description"}>
          <strong>BRK Group</strong>
          <p>
            Event host for corona vaccination, Tele2 and Friends arena.
          </p>
        </div>
      </div>
      <div className={"experience-part"}>
        <div className={"experience-year"}>
          <p>2021-2022</p>
        </div>
        <div className={"experience-description"}>
          <strong>JPA and Fållan</strong>
          <p>
            Working at a club wardrobe
          </p>
        </div>
      </div>
      <div className={"experience-part"}>
        <div className={"experience-year"}>
          <p>2021-2022</p>
        </div>
        <div className={"experience-description"}>
          <strong>Viasales</strong>
          <p>
            Door to door selling, fiber services that included TV, streaming and broadband
          </p>
        </div>
      </div>
      <div className={"small-experience-title"}>
        <div className={"small-experience-icon-box"}>
          <FontAwesomeIcon icon={faDigging} size="1x"/>
        </div>
        <p>Sparetime</p>
      </div>
      <div className={"experience-part"}>
        <div className={"experience-year"}>
          <p>2018-2023</p>
        </div>
        <div className={"experience-description"}>
          <strong>Coaching</strong>
          <p>
            Playing at high ratings gave me the opportunity to join boosting communities and help players become better using voice
          </p>
        </div>
      </div>
    </Tilt>
  )
}

export default Experience;