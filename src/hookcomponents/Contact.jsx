import { useState, useRef } from 'react';
import emailjs from '@emailjs/browser';
import Tilt from 'react-parallax-tilt';
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faLinkedin, faSkype, faDiscord, faWhatsappSquare, faGitlabSquare } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import swedenDotted from '../images/swedenDotted.png';
import '../css/Contact.css';

const Contact = () => {
	const[firstName, setFirstName] = useState("")
	const[lastName, setLastName] = useState("")
	const[email, setEmail] = useState("")
	const[country, setCountry] = useState("Sweden")
	const[message, setMessage] = useState("")
  const form = useRef();

  const handleSubmit = (event) => {
    event.preventDefault();
    alert(firstName + " " + lastName + " " + email + " " + country + " " + message)
		setFirstName("");
		setLastName("");
		setEmail("");
		setCountry("Sweden");
		setMessage("");

    emailjs.sendForm('service_dlynk2d',
     'template_99dbvaj',
      form.current,
      'EkttUeStV4RW1sZkh')
    .then((result) => {
        console.log(result.text);
    }, (error) => {
        console.log(error.text);
    });
	}
  
  return (
    <div className={"contact-section"}>
      <div className={"cross"}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
      <h2>Contact Me</h2>
      <div className={"contact-wrapper"}>
        <div className={"contact-section-extra"}>
          <h4>I would love to hear from you!</h4>
          <h3>Email</h3>
          <div className={"contact-section-icons"}>
            <div className={"contact-section-icons-information"} onClick={() => {navigator.clipboard.writeText("eduard.x.ybarra@gmail.com")}}>
              <FontAwesomeIcon icon={faEnvelope} size="3x"/>
              <h6>eduard.x.ybarra@gmail.com</h6>
            </div>
          </div>
          <h3>Web Links</h3>
          <div className={"contact-section-icons"}>
            <a href="https://www.linkedin.com/in/eduard-ybarra-a49459137/" target="_blank">
              <FontAwesomeIcon icon={faLinkedin} size="3x"/>
            </a>
            <a href="https://www.gitlab.com/eduard.x.ybarra/eyportfolio" target="_blank">
              <FontAwesomeIcon icon={faGitlabSquare} size="3x"/>
            </a>
          </div>
          <h3>Chat Applications</h3>
          <div className={"contact-section-icons"}>
            <div className={"contact-section-icons-information"} onClick={() => {navigator.clipboard.writeText("+46 702266077")}}>
              <FontAwesomeIcon icon={faWhatsappSquare} size="3x"/>
              <h6>+46 702266077</h6>
            </div>
            <div className={"contact-section-icons-information"} onClick={() => {navigator.clipboard.writeText("forthelow#4336")}}>
              <FontAwesomeIcon icon={faDiscord} size="3x"/>
              <h6>forthelow#4336</h6>
            </div>
            <div className={"contact-section-icons-information"} onClick={() => {navigator.clipboard.writeText("forthelow")}}>
              <FontAwesomeIcon icon={faSkype} size="3x"/>
              <h6>forthelow</h6>
            </div>
          </div>
        </div>
        <form className={"contact-form"} ref={form} onSubmit={handleSubmit}>
          <Tilt transitionSpeed={5000} tiltEnable={false} glareEnable={true} glareMaxOpacity={0.3} glareColor="white" glareBorderRadius="20px" glarePosition='all' style={{height: "470px"}}>
            <div className={"contact-section-concatenate"}>
              <div className={"contact-section-names"}>
                <div className={"contact-names-input"}>
                  <label>
                  First Name:
                  </label>
                  <input type="text" name="user_name" value={firstName} placeholder="First Name..." required onChange={(e) => setFirstName(e.target.value)} />
                </div>
                <div className={"contact-names-input"}>
                  <label>
                  Last Name:
                  </label>
                  <input type="text" name="user_lastname" value={lastName} placeholder="Last Name..." required onChange={(e) => setLastName(e.target.value)} />
                </div>
              </div>
              <div className={"contact-section-names"}>
                <div className={"contact-names-input"}>
                  <label>
                  Email:
                  </label>
                  <input type="email" name="user_email" value={email} placeholder="Email..." required onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div className={"contact-names-input"}>
                  <label>
                  Country
                  </label>
                  <select id="country" name="country" onChange={(e) => setCountry(e.target.value)}>
                    <option value="Sweden">Sweden</option>
                    <option value="Other">Other</option>
                  </select>
                </div>
              </div>
              <div className={"contact-names-input"}>
                <label>
                Message:		
                </label>
                <textarea name="message" value={message} onChange={(e) => setMessage(e.target.value)} ></textarea>
              </div>
              <input className={"contact-section-submit"} type="submit" value="Submit"/>
            </div>
          </Tilt>
        </form>
        <div className={"contact-map"}>
          <img className={"dotted-map"} src={swedenDotted} alt=""></img>
          <div className={"dotted-location"}></div>
          <div className={"information-location"}>
            <h5>Stockholm</h5>
            <span>Södra Jordbrovägen 167</span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Contact;