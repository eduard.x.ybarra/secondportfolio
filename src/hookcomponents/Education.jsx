import React from 'react';
import Tilt from 'react-parallax-tilt';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDigging, faBriefcase, faSchool, faUserGraduate } from "@fortawesome/free-solid-svg-icons";
import '../css/Education.css';

const Education = () => {
  return (
    <Tilt transitionSpeed={5000} tiltEnable={false} glareEnable={true} glareMaxOpacity={0.1} glareColor="white" glarePosition='all' className={"education-section"}>
      <div className={"education-title"}>
        <div className={"education-icon-box"}>
          <FontAwesomeIcon icon={faUserGraduate} size="2x"/>
        </div>
        <p>Education</p>
      </div>
      <div className={"small-education-title"}>
        <div className={"small-education-icon-box"}>
          <FontAwesomeIcon icon={faSchool} size="1x"/>
        </div>
        <p>Schools</p>
      </div>
      <div className={"education-part"}>
        <div className={"education-year"}>
          <p>2006-2010</p>
        </div>
        <div className={"education-description"}>
          <strong>Rudan Skolan</strong>
          <p>
            Basic grading
          </p>
        </div>
      </div>
      <div className={"education-part"}>
        <div className={"education-year"}>
          <p>2010-2013</p>
        </div>
        <div className={"education-description"}>
          <strong>Fredrika Bremer</strong>
          <p>
            Teknik el- och programmering with physics and chemistry
          </p>
        </div>
      </div>
      <div className={"education-part"}>
        <div className={"education-year"}>
          <p>2017-2019</p>
        </div>
        <div className={"education-description"}>
          <strong>YH C3L</strong>
          <p>
            Specialization in Java and Javascript
          </p>
        </div>
      </div>
      <div className={"small-education-title"}>
        <div className={"small-education-icon-box"}>
          <FontAwesomeIcon icon={faDigging} size="1x"/>
        </div>
        <p>Self-Taught</p>
      </div>
      <div className={"education-part"}>
        <div className={"education-year"}>
          <p>2022-2023</p>
        </div>
        <div className={"education-description"}>
          <strong>Javascript React Projects</strong>
          <p>
            Created portfolios with various content
          </p>
        </div>
      </div>
    </Tilt>
  )
}

export default Education;